import React from "react";

const Label = () => {
  return (
    <div>
      <svg
        width="14"
        height="14"
        viewBox="0 0 14 14"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M12.0563 7.74836L7.75433 12.0504C7.52926 12.2757 7.22383 12.4023 6.90533 12.4023C6.58683 12.4023 6.28143 12.2757 6.05633 12.0504L0.666626 6.66666V0.666656H6.66663L12.0563 6.05636C12.5216 6.52439 12.5216 7.28032 12.0563 7.74836Z"
          stroke="#67809A"
          strokeWidth="1.33333"
          strokeLinejoin="round"
        />
      </svg>
    </div>
  );
};

export default Label;
