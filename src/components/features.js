import axios from 'axios'
export async function getDoctors() {
    try {
        const response = await axios.get('https://script.google.com/macros/s/AKfycbwSVMv2cLveWi16_qi1cXhl483-aprVyuOrBf_EL9qYxjahZl69CP29bFGOQnrGA8kG/exec')
        return response.data
    }
    catch (e) {
        return e
    }
}
export function getSale(price, discount) {
    return ((discount / price) * 100).toFixed();
}
export const dateOptions = {
    month: "short",
    day: "numeric",
    hour: "numeric",
    hour12: true,
};

const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
export const checkDayForAvailable = (day, unavailable_dates, unavailable_days) => {
    return (unavailable_dates.includes(day.getDate()) || unavailable_days.includes(days[day.getDay()]))
}

export function getStartDate(unavailable_dates, unavailable_days, start_hour) {
    let availableDay = new Date();
    availableDay.setMilliseconds(0)
    availableDay.setSeconds(0)
    const unavailableDates = unavailable_dates.map((date) => new Date(date).getDate())
    while (unavailableDates.includes(availableDay.getDate()) || unavailable_days.includes(days[availableDay.getDay()])) {
        availableDay.setDate(availableDay.getDate() + 1)
    }
    availableDay.setHours(start_hour.split(':')[0])
    availableDay.setMinutes(start_hour.split(':')[1])
    return availableDay
}
