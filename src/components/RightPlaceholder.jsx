import React from "react";

export default function RightPlaceholder() {
  return (
    <>
      <aside className="side side-right">
        <div className="placeholder-2"></div>
        <div className="placeholder-3"></div>
      </aside>
    </>
  );
}
