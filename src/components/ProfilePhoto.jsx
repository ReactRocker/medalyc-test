/* eslint-disable jsx-a11y/img-redundant-alt */
import React from "react";

export default function ProfilePhoto({ img }) {
  return (
    <div className="photo-block">
      <img className="profile-photo" src={img} alt="doctor-picture"></img>
    </div>
  );
}
