import React, { useState, useEffect } from 'react';
import { Description, ProfilePhoto } from '../';
import '../../styles/doctor.scss';
import Schedule from '../Schedule/Schedule';
import { dateOptions, getStartDate } from '../features';

export default function Doctor({ doctor }) {
  const [currentPrice, setCurrentPrice] = useState();
  const [hasSale, setHasSale] = useState(false);
  const [hover, setHover] = useState(false);
  const startDate = getStartDate(
    doctor.unavailable_dates,
    doctor.unavailable_days,
    doctor.appointments_start_hour
  );
  useEffect(() => {
    if (doctor.fee_discount) {
      setCurrentPrice(doctor.fee_amount - doctor.fee_discount);
      setHasSale(true);
    } else {
      setCurrentPrice(doctor.fee_amount);
    }
  }, [doctor]);
  const [openSchedule, setOpenSchedule] = useState(false);
  const handleClick = (e) => {
    e.preventDefault();
    setOpenSchedule(!openSchedule);
  };
  return (
    <section
      data-testid="doctor-card"
      className={`doctor-card ${openSchedule ? 'card-open' : ''} `}
      style={{ backgroundColor: hover ? '#f0f2f5' : '#fff' }}
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
      // onMouseLeave={}
    >
      <div className="doctor-info flex">
        <ProfilePhoto img={doctor.profile_pic} />
        <Description
          ratingAvg={doctor.rating_avg}
          reviewsCount={doctor.review_count}
          feeCurrency={doctor.fee_currency}
          currentPrice={currentPrice}
          feeDiscount={doctor.fee_discount}
          clinicName={doctor.clinic_name}
          clinicLocation={doctor.clinic_location}
          name={doctor.name}
          position={doctor.position}
          specialty={doctor.specialty}
          patientsCount={doctor.patients_count}
          hasSale={hasSale}
          feeAmount={doctor.fee_amount}
        />
      </div>
      <div className="dashboard">
        {!openSchedule ? (
          <div className="dash-header flex justify-between">
            <h3>
              Available From:
              {startDate
                .toLocaleDateString('en-US', dateOptions)
                .replace('AM', 'am')
                .replace('PM', 'pm')}
              {} <span className="available-date">{}</span>
            </h3>
            <button className="open" onClick={handleClick}>
              {' '}
              See Full Schedule <i className="down"></i>
            </button>
          </div>
        ) : (
          <Schedule
            doctor={doctor}
            startDay={startDate}
            isOpen={openSchedule}
            handleClose={handleClick}
            unavailable_dates={doctor.unavailable_dates}
            unavailableDays={doctor.unavailable_days}
            appointmentsEndHour={doctor.appointments_end_hour}
            appointmentsStartHour={doctor.appointments_start_hour}
            appointmentsDurationMinutes={doctor.appointment_duration_minutes}
            bookedAppointments={doctor.booked_appointments}
          />
        )}
      </div>
    </section>
  );
}
