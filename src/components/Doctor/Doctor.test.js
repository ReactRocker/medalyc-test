import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';
import React from 'react';
import Doctor from './Doctor';
import '../../styles/doctor.scss';

const fakeDoctor = {
  id: 1001,
  name: 'Michael D. Brochu',
  profile_pic:
    'https://drive.google.com/uc?id=1bqEmq5qzVD87RCobVJv972Tpk8V-zuGT&export=view',
  specialty: 'Dermatologist',
  position: 'Professor of dermatology at Cambridge Medical School',
  clinic_name: 'ABC advanced clinic',
  clinic_location: 'Jeddah - KSA',
  rating_avg: 4.879,
  reviews_count: 346,
  fee_currency: 'SAR',
  fee_amount: 300,
  fee_discount: 50,
  patients_count: 345,
  unavailable_days: ['Sunday'],
  unavailable_dates: ['2022-02-08T00:00', '2022-02-10T00:00'],
  appointments_start_hour: '09:00',
  appointments_end_hour: '18:00',
  appointment_duration_minutes: 30,
  booked_appointments: ['2022-02-10T09:00', '2022-02-17T09:00 '],
};

describe('Doctor Card', () => {
  test('should change background color on hover event', () => {
    render(<Doctor doctor={fakeDoctor} />);

    const DoctorCard = screen.getByTestId('doctor-card');
    expect(DoctorCard).toHaveStyle('background-color: rgb(255, 255, 255)');

    fireEvent.mouseOver(DoctorCard);
    expect(DoctorCard).toHaveStyle('background-color: #f0f2f5');
  });
});
