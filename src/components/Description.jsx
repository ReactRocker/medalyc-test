import React from "react";
import { PeopleGroup, Ticket } from "../svg";
import { getSale } from "./features";

export default function Description({
  ratingAvg,
  reviewsCount,
  feeCurrency,
  currentPrice,
  feeDiscount,
  clinicName,
  clinicLocation,
  name,
  position,
  specialty,
  patientsCount,
  hasSale,
  feeAmount,
}) {
  return (
    <div className="profile-card flex flex-col justify-between">
      <ul className="statistic flex justify-start items-center">
        <li className="rating-avg text-center">{ratingAvg.toFixed(1)}</li>
        <li className="review">({reviewsCount} Review)</li>
      </ul>
      <h1 className="name">{name}</h1>
      <h2 className="specialty">{specialty}</h2>
      <h3 className="position">{position}</h3>
      <ul className="for-patient flex items-center">
        <li className="patient-count mr-2 flex ">
          <PeopleGroup /> {patientsCount || 0} Patient
        </li>
        <Ticket />
        {hasSale ? (
          <li>
            <span className="default-price line-through ml-2">
              {feeAmount} {feeCurrency}
            </span>{" "}
            -{" "}
            <span className="current-price">
              {currentPrice} {feeCurrency}
            </span>
            <span className="sale">-{getSale(feeAmount, feeDiscount)}%</span>
          </li>
        ) : (
          <li className="current-price ml-2">
            {feeAmount} {feeCurrency}
          </li>
        )}
      </ul>
      <ul className="flex align-middle clinic">
        <li className="clinic-name">{clinicName}</li>
        <li className="location">{clinicLocation}</li>
      </ul>
    </div>
  );
}
