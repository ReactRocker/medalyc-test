import React from "react";

const TimeCalendar = ({ appointments }) => {
  return (
    <div>
      <ul className="flex flex-col">
        {appointments.map((appointment) => (
          <li
            key={appointment.time.toLocaleString()}
            className={`${appointment.isBooked ? "bg-red-400" : ""}`}
          >
            {appointment.time.toLocaleString()}
          </li>
        ))}
      </ul>
    </div>
  );
};
export default React.memo(TimeCalendar);
