import React from "react";
import { formatDate } from "./features";

const DayPointer = ({ date, handleClick, freeAppointments }) => {
  return (
    <>
      <button onClick={() => handleClick(date)}>{formatDate(date)}</button>
      <span className="bg-orange-500 text-white p-1 text-center rounded-xl ml-4">
        {freeAppointments}
      </span>
    </>
  );
};

export default React.memo(DayPointer);
