import { checkDayForAvailable } from "../features";
import { dateOptions } from '../features';

export function formatDate(date) {
    const tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    if (date.toDateString() === new Date().toDateString()) {
        return "Today";
    } else if (date.toDateString() === tomorrow.toDateString()) {
        return "Tomorrow";
    } else {
        return date.toLocaleDateString("en-US", dateOptions);
    }
}
export const dayBack = (prevDays, unavailableDates, unavailable_days) => {
    const newDates = prevDays;
    let lastDay = new Date(newDates[0]);
    newDates.pop();
    lastDay.setDate(lastDay.getDate() - 1);
    while (checkDayForAvailable(lastDay, unavailableDates, unavailable_days)) {
        lastDay.setDate(lastDay.getDate() - 1);
    }
    newDates.unshift(lastDay);
    return newDates;
};
export const dayForward = (prevDays, unavailableDates, unavailable_days) => {
    const newDates = prevDays;
    let lastDay = new Date(prevDays[prevDays.length - 1]);
    newDates.shift();
    lastDay.setDate(lastDay.getDate() + 1);
    while (checkDayForAvailable(lastDay, unavailableDates, unavailable_days)) {
        lastDay.setDate(lastDay.getDate() + 1);
    }
    newDates.push(lastDay);
    return newDates;
};
export const createInitState = (startDay, unavailableDates, unavailableDays) => {
    const freeDates = [startDay];
    while (freeDates.length !== 4) {
        let newDate = new Date(freeDates[freeDates.length - 1].getTime());
        newDate.setDate(newDate.getDate() + 1);
        while (
            checkDayForAvailable(newDate, unavailableDates, unavailableDays)
        ) {
            newDate.setDate(newDate.getDate() + 1);
        }
        freeDates.push(newDate);
    }
    return freeDates
}
export const getAvailableAppointments = (
    date,
    appointmentsEndHour,
    bookedAppointmentsString,
    appointmentsDurationMinutes,
    appointmentsStartHour
) => {
    let receptionTime = new Date(date);
    const [startHour, startMinutes] = appointmentsStartHour.split(':')
    receptionTime.setHours(parseInt(startHour));
    receptionTime.setMinutes(parseInt(startMinutes));

    const [finishedHours, finishedMinutes] = appointmentsEndHour.split(":");
    const lastAppointment = new Date(date);
    lastAppointment.setHours(parseInt(finishedHours));

    lastAppointment.setMinutes(parseInt(finishedMinutes));
    let bookedAppointments = bookedAppointmentsString.map((dateTime) =>
        new Date(dateTime.trim()).getTime()
    );
    const appointmentsForDay = [];
    while (receptionTime.getTime() <= lastAppointment.getTime()) {
        receptionTime = new Date(receptionTime);
        receptionTime.setMinutes(
            receptionTime.getMinutes() + parseInt(appointmentsDurationMinutes)
        );
        if (bookedAppointments.includes(receptionTime.getTime())) {

            appointmentsForDay.push({ time: receptionTime, isBooked: true })
        }
        else {
            appointmentsForDay.push({ time: receptionTime, isBooked: false });

        }
    }

    return appointmentsForDay
};