import React, { useEffect, useState } from "react";
import { TimeCalendar } from "..";
import DayPointer from "./DayPointer";
import {
  dayForward,
  dayBack,
  createInitState,
  getAvailableAppointments,
} from "./features";

export default function Schedule({
  handleClose,
  startDay,
  unavailableDays,
  unavailable_dates,
  appointmentsEndHour,
  appointmentsStartHour,
  appointmentsDurationMinutes,
  bookedAppointments,
}) {
  const [receptionDays, setReceptionDays] = useState([]);
  const [unavailableDates, setUnavailableDates] = useState(
    unavailable_dates.map((date) => new Date(date).getDate())
  );
  const [allAppointments, setAllAppointments] = useState({});
  const [currentDate, setCurrentDate] = useState(startDay);
  const [currentAppointments, setCurrentAppointments] = useState([]);
  console.log(currentDate);
  useEffect(() => {
    setReceptionDays(
      createInitState(
        startDay,
        unavailableDates,
        unavailableDays,
        appointmentsStartHour
      )
    );
  }, [appointmentsStartHour, startDay, unavailableDates, unavailableDays]);
  useEffect(() => {
    setAllAppointments(
      receptionDays.reduce(
        (obj, date) => ({
          ...obj,
          [date.toString()]: getAvailableAppointments(
            date,
            appointmentsEndHour,
            bookedAppointments,
            appointmentsDurationMinutes,
            appointmentsStartHour
          ),
        }),
        {}
      )
    );
  }, [receptionDays]);
  const dayMoveForward = () => {
    setReceptionDays((prev) =>
      dayForward([...prev], unavailableDates, unavailableDays)
    );
  };

  const pickDay = (day) => {
    setCurrentDate(day);
    setCurrentAppointments(allAppointments[day]);
  };
  const dayMoveBack = () => {
    setReceptionDays((prev) =>
      dayBack([...prev], unavailableDates, unavailableDays)
    );
  };
  console.log(receptionDays);
  return (
    <div className="schedule flex-col">
      <div className="header flex justify-between">
        <div className="day-bar flex">
          <button onClick={dayMoveBack} disabled={allAppointments[startDay]}>
            left
          </button>

          <ul className="day-bar flex ">
            {receptionDays &&
              receptionDays.map((date) => (
                <li
                  className={`day ${
                    date.getTime() === currentDate.getTime() ? "active" : ""
                  }`}
                  key={`${date}`}
                >
                  <DayPointer
                    date={date}
                    handleClick={pickDay}
                    freeAppointments={
                      allAppointments[date] &&
                      allAppointments[date].reduce(
                        (sum, el) => (el.isBooked ? sum : (sum += 1)),
                        0
                      )
                    }
                  />
                </li>
              ))}
          </ul>
          <button onClick={dayMoveForward}>Right</button>
        </div>
        <button className="close" onClick={handleClose}>
          Close
        </button>
      </div>
      {currentAppointments && (
        <TimeCalendar appointments={currentAppointments}></TimeCalendar>
      )}
    </div>
  );
}
