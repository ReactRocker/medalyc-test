import React from 'react';
import Doctor from './Doctor';
import '../styles/doctors.scss';
export default function Doctors({ doctors }) {
  return (
    <>
      {doctors && (
        <ul className="doctors">
          <h1>List of doctors</h1>

          {doctors.map((doctor) => (
            <Doctor key={`${doctor.name} ${doctor.id}`} doctor={doctor} />
          ))}
        </ul>
      )}
    </>
  );
}
