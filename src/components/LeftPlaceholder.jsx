import React from "react";

export default function RightPlaceholder() {
  return (
    <>
      <aside className="side side-left">
        <div className="placeholder-1"></div>
      </aside>
    </>
  );
}
