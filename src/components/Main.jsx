import React from "react";
import { LeftPlaceholder, RightPlaceholder } from ".";
import { useEffect, useState } from "react";
import { getDoctors } from "./features";
import { Doctors } from ".";
const Main = () => {
  const [doctors, setDoctors] = useState([]);
  useEffect(() => {
    getDoctors().then((data) => setDoctors(data));
  }, []);
  return (
    <main>
      <LeftPlaceholder />
      <Doctors doctors={doctors} />
      <RightPlaceholder />
    </main>
  );
};

export default Main;
