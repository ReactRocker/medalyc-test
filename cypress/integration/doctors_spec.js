describe('doctors', () => {
  it('user can see list of 10 doctors', () => {
    const ExpectedLength = 10;
    cy.visit('http://localhost:3000/');
    cy.get('.doctor-card').then((listing) => {
      expect(listing).to.have.length(ExpectedLength);
    });
  });
});
